# Web server viết bằng ngôn ngữ lập trình [Rust](https://rust-lang.org)
## Dành cho việc học là chính

Minimum Rust version 1.39

```sh
cargo run --release
```