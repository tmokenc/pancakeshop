#[macro_use]
extern crate actix_web;

use std::env;
use actix_web::{middleware, App, HttpServer, HttpResponse};

type Result<T> = std::result::Result<T, Box<dyn std::error::Error>>;

#[get("/")]
async fn index() -> HttpResponse {
    HttpResponse::Ok().body(include_str!("../static/Index.html"))
}


#[actix_rt::main]
async fn main() -> Result<()> {
    if env::var("RUST_LOG").is_err() {
        env::set_var("RUST_LOG", "actix_web=info")
    }
    
    let ip = env::var("IP").unwrap_or("0.0.0.0:80".to_string());
    println!("IP: {}", &ip);
    
    env_logger::init();
    HttpServer::new(|| {
        App::new()
            .wrap(middleware::Logger::default())
            .service(index)
            .service(actix_files::Files::new("/", "./static"))
    })
    .bind(ip)?
    .start()
    .await?;
        
    Ok(())
}